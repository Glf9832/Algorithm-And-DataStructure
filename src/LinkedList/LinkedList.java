package LinkedList;

class ListNode{
    int val;
    ListNode next;

    ListNode(int val){
        this.val = val;
        this.next = null;
    }
}

public class LinkedList {

    public static void main(String[] args){
        ListNode head = CreateList();

        Search(head);

        Insert(head,3,new ListNode(100));

        System.out.println("\n");
        Search(head);

        Delete(head,3);

        System.out.println("\n");
        Search(head);
    }

    public static ListNode CreateList(){
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(2);
        ListNode n3 = new ListNode(3);
        ListNode n4 = new ListNode(4);
        ListNode n5 = new ListNode(5);
        ListNode n6 = new ListNode(6);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        n5.next = n6;
        return n1;
    }

    public static void Search(ListNode head){
        if (head != null && head.next != null){
            System.out.println(head.val);
            Search(head.next);
        }else if (head.next ==null){
            System.out.println(head.val);
        }
    }

    public static void Insert(ListNode head,int post,ListNode node){
        for(int i=0;i<post-1;i++){
            head = head.next;
        }
        node.next = head.next;
        head.next = node;
    }

    public static void Delete(ListNode head,int post){
        for(int i=0;i<post-1;i++){
            head = head.next;
        }
        ListNode temp = head.next;
        head.next = temp.next;
        temp = null;
    }
}
